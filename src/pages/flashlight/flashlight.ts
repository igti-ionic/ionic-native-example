import { Flashlight } from '@ionic-native/flashlight';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-flashlight',
  templateUrl: 'flashlight.html'
})
export class FlashlightPage {
  acao: string = 'Ligar';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public lanterna: Flashlight
  ) {}

  toggleLanterna() {
    /**
     * O método 'toggle' liga se a lanter estiver desligada
     * e desliga se a lanterna estiver ligada
     */
    this.lanterna.toggle();

    /**
     * Texto do botão
     */
    this.acao = this.lanterna.isSwitchedOn() ? 'Desligar' : 'Ligar';
  }
}
