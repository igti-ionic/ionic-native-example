import { NgModule } from '@angular/core';
import { Flashlight } from '@ionic-native/flashlight';
import { IonicPageModule } from 'ionic-angular';
import { FlashlightPage } from './flashlight';

@NgModule({
  declarations: [
    FlashlightPage,
  ],
  imports: [
    IonicPageModule.forChild(FlashlightPage),
  ],
  providers: [
    Flashlight,
  ]
})
export class FlashlightPageModule {}
