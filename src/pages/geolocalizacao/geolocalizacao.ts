import { Component, NgZone } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { Geolocation } from '@ionic-native/geolocation';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@IonicPage()
@Component({
  selector: 'page-geolocalizacao',
  templateUrl: 'geolocalizacao.html'
})
export class GeolocalizacaoPage {
  coordenadas: any = {};
  endereco: any = {};

  /**
   * Configurações
   * para o sensor de
   * GPS
   */
  opcoes = {
    enableHighAccuracy: true,
    timeout: 30000,
    maximumAge: 0
  };

  constructor(
    public geolocation: Geolocation,
    public http: Http,
    public platform: Platform,
    public zone: NgZone
  ) {}

  public verificarCoordenadas(latitude, longitude) {
    this.coordenadas.latitude = latitude;
    this.coordenadas.longitude = longitude;
    this.geolocalizacaoReversa();
  }

  public lerGPS() {
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition(this.opcoes).then(resp => {
        this.coordenadas.latitude = resp.coords.latitude;
        this.coordenadas.longitude = resp.coords.longitude;
        this.geolocalizacaoReversa();
      });
    });
  }

  geolocalizacaoReversa() {
    const url =
      `https://nominatim.openstreetmap.org/reverse?format=json&lat=` +
      `${this.coordenadas.latitude}&lon=${this.coordenadas.longitude}` +
      `&zoom=18&addressdetails=1`;

    console.log(url);

    this.http
      .get(url)
      .map(res => res.json())
      .toPromise()
      .then(data => {
        console.log('Data', data);
        this.zone.run(() => {
          this.endereco = data.address;
        });
      });
  }
}
