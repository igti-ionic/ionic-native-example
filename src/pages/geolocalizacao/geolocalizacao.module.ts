import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { GeolocalizacaoPage } from './geolocalizacao';

@NgModule({
  declarations: [
    GeolocalizacaoPage,
  ],
  imports: [
    HttpModule,
    IonicPageModule.forChild(GeolocalizacaoPage),
  ],
  providers: [
    Geolocation,
  ]
})
export class GeolocalizacaoPageModule {}
