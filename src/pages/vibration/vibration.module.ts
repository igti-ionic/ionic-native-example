import { NgModule } from '@angular/core';
import { Vibration } from '@ionic-native/vibration';

import { IonicPageModule } from 'ionic-angular';
import { VibrationPage } from './vibration';

@NgModule({
  declarations: [
    VibrationPage,
  ],
  imports: [
    IonicPageModule.forChild(VibrationPage),
  ],
  providers: [
    Vibration,
  ]
})
export class VibrationPageModule {}
