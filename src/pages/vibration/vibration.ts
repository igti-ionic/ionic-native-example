import { Vibration } from '@ionic-native/vibration';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-vibration',
  templateUrl: 'vibration.html',
})
export class VibrationPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public vibration: Vibration,
  ) {}

  vibrarDispositivo(tempo: number) {
    this.vibration.vibrate(tempo);
  }
}
