import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import {
  BatteryStatus,
  BatteryStatusResponse
} from '@ionic-native/battery-status';

@IonicPage()
@Component({
  selector: 'page-battery',
  templateUrl: 'battery.html'
})
export class BatteryPage {
  /**
   * Atributos
   */
  batterySubscription: any;
  statusBateria: BatteryStatusResponse;
  batteryColor: string = 'dark';

  /**
   * Construtor com injeções de dependência
   * @param navCtrl
   * @param navParams
   * @param bateria
   * @param platform
   * @param zone
   */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public bateria: BatteryStatus,
    public platform: Platform,
    public zone: NgZone
  ) {
    this.verificarBateria();
  }

  /**
   * Definindo cor conforme o status
   * da bateria
   */
  private validarBateria() {
    if (this.statusBateria.level <= 20) {
      this.batteryColor = 'danger';
      return;
    }
    if (this.statusBateria.level <= 90) {
      this.batteryColor = 'primary';
      return;
    }
    this.batteryColor = 'secondary';
  }

  public verificarBateria() {
    /**
     * Quando a plataforma estiver "pronta"...
     */
    this.platform.ready().then(() => {
      /**
       * Inscrição no serviço de bateria.
       * Havendo mudança, obtemos os dados
       * da bateria e atualizamos a tela com
       * o apoio do objeto "zone", que sincroniza
       * ações feitas externamente (leitura da bateria)
       * com a interface
       */
      this.batterySubscription = this.bateria
        .onChange()
        .subscribe(batteryData => {
          this.zone.run(() => {
            this.statusBateria = batteryData;
            this.validarBateria();
          });
        });
    });
  }

  ionViewWillUnload() {
    /**
     * Quando fazemos inscrição em algum
     * serviço (subscribe), é uma boa
     * prática nos desinscrevermos (unsubscribe)
     * quando a tela for encerrada. O melhor método
     * do ciclo de vida do Ionic para a desinscrição
     * é o ionViewWillUnload
     */
    this.batterySubscription.unsubscribe();
  }
}
